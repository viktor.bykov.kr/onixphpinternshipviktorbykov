<?php

class User{
    private $name;
    private $balance;

    function __construct($name, $balance){
        $this->name = $name;
        $this->balance = $balance;
    }

    private function getName(){
        return $this->name;
    }

    public function giveMoney($amount, &$recipient){
        if ($amount <= $this->balance) {
            $this->balance -= $amount;
            $recipient->takeMoney($amount);
            echo "Пользователь {$this->name} перечислил {$amount} пользователю {$recipient->getName()}<br>";
        } else {
            echo "Ошибка! Недостаточно денег для перечисления<br>";
        }
    }

    private function takeMoney($amount){
        $this->balance += $amount;
    }

    public function printStatus(){
        echo "У пользователя {$this->name} сейчас на счету {$this->balance}<br>";
    }
}

$user = new User("Petya", 2000);
$user2 = new User("Vasya", 3000);

$user->printStatus();
$user2->printStatus();

$user->giveMoney(1000, $user2);

$user->printStatus();
$user2->printStatus();
