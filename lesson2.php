<?php

namespace lesson2;

abstract class Product
{
    private $name;
    private $price;
    private $owner;
    private static $products = array(); 

    public function __construct($name, $price, $owner)
    {
        $this->name = $name;
        $this->price = $price;
        $this->owner = $owner;
    }

    public static function registerProduct($prod, $user)
    {
        $isPresent = false;
        foreach (self::$products as $element) {
            if ($prod instanceof $element) {
                $isPresent = true;
            }
        }
        if (!$isPresent) {
            self::$products[] = $prod;
            // TODO: add user
        } else {
            echo "Class is present<br><br>";
        }
    }


    public static function iterateProducts(){
        return new class implements \Iterator {
            private $var = array();

            public function __construct($products)
            {
                if (is_array($products)) {
                    $this->var = $products;
                }
            }
        
            public function rewind()
            {
                echo "перемотка в начало\n";
                reset($this->var);
            }
          
            public function current()
            {
                $var = current($this->var);
                echo "текущий: $var\n";
                return $var;
            }
          
            public function key() 
            {
                $var = key($this->var);
                echo "ключ: $var\n";
                return $var;
            }
          
            public function next() 
            {
                $var = next($this->var);
                echo "следующий: $var\n";
                return $var;
            }
          
            public function valid()
            {
                $key = key($this->var);
                $var = ($key !== NULL && $key !== FALSE);
                echo "верный: $var\n";
                return $var;
            }
        };
    }
    
    public static function print()
    {
        echo "<br><br>";
       var_dump(self::$products);
        echo "<br><br>";
    }
}



class Processor extends Product 
{
    private $frequency;

    function __construct($frequency)
    {
        $this->frequency = $frequency;
    }
}

class Ram extends Product
{
    private $type;
    private $memory;

    function __construct($type, $memory)
    {
        $this->type = $type;
        $this->memory = $memory;
    }
}

abstract class ErrorLog
{
    private $log = array();

    function writeError($msg)
    {
        $this->log[] = $msg;
    }
    public static function displayError()
    {
        var_dump(self::$log);
        return self::$log;
    }
}

class User
{
    private $name;
    private $balance;

    function __construct($name, $balance)
    {
        $this->name = $name;
        $this->balance = $balance;
    }

    private function getName()
    {
        return $this->name;
    }

    public function giveMoney($amount, &$recipient)
    {
        if ($amount <= $this->balance) {
            $this->balance -= $amount;
            $recipient->takeMoney($amount);
            echo "Пользователь {$this->name} перечислил {$amount} пользователю {$recipient->getName()}<br>";
        } else {
            $errorLog = new errorLog();
            $errorLog->writeError("Ошибка! Недостаточно денег для перечисления<br>");
            $errorLog->displayError();
        }
    }

    private function takeMoney($amount)
    {
        $this->balance += $amount;
    }

    public function __toString()
    {
        echo "У пользователя {$this->name} сейчас на счету {$this->balance}<br>";
    }
}

$ram =  new Ram("rama", "11");
$proc = new Processor("3G");
$proc2 = new Processor("4G");
$proc3 = new Processor("5G");
$user = new User("Petya", 2000);
// Product::print();
Product::registerProduct($ram, $user);
// Product::print();
Product::registerProduct($proc, $user);
Product::print();
// Product::registerProduct($proc);
// ErrorLog::displayError();
// Product::print();
// Product::iterateProducts();
// $proc->print();
// $proc2->registerProduct($proc);
// $proc2->registerProduct($ram);
// $proc2->registerProduct($proc3);
// $proc2->print();

// $user = new User("Petya", 2000);
// $user2 = new User("Vasya", 3000);
 
// $user->__toString();
// $user2->__toString();
 
// $user->giveMoney(4000, $user2);
// $user->giveMoney(4000, $user2);
 
// $user->__toString();
// $user2->__toString();